<?php
namespace App\Tests;

use App\App;
use PHPUnit\Framework\TestCase;


class AppTest extends TestCase
{
    public function testReturnedStrValue()
    {
        $this->assertEquals('World', App::getStr());
    }
}
